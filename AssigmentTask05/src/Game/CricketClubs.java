package Game;

public class CricketClubs implements Comparable<CricketClubs> {
  private int position;
  private String club_names;
  private int matchesplayed;
  private int matcheswon;
  private int matchesdrawn;
  private int matcheslost;
  private int pointscored;
  private int disqualifedperMember;
  private int totalwon;
  private int totallost;
  private int centuries;
  private int halfcenturies;
  private int sixes;
  private int wiketstaken;


  public CricketClubs(int position, String club_names, int matchesplayed, int matcheswon, 
		  int matchesdrawn,int matcheslost, int pointscored, int disqualifedperMember, 
		  int totalwon, int totallost,int centuries,int halfcenturies, int sixes, int wiketstaken) 
  {
    this.position = position;
    this.club_names = club_names;
    this.matchesplayed = matchesplayed;
    this.matcheswon = matcheswon;
    this.matchesdrawn = matchesdrawn;
    this.matcheslost = matcheslost;
    this.pointscored = pointscored;
    this.disqualifedperMember = disqualifedperMember;
    this.totalwon = totalwon;
    this.totallost = totallost;
    this.centuries = centuries;
    this.halfcenturies = halfcenturies;
    this.sixes = sixes;
    this.wiketstaken = wiketstaken;
   
  }

  public String toString() {
	  return String.format("%-3d%-20s%10d%10d%10d%10d", position, club_names, pointscored, 
			  totalwon, totallost,matchesdrawn);
  }
  

  public int getPosition() {
    return position;
  }

  public void setPosition(int position) {
    this.position = position;
  }

  public String getClub() {
    return club_names;
  }

  public void setClub(String club) {
    this.club_names = club;
  }

  public int getPlayed() {
    return matchesplayed;
  }

  public void setPlayed(int played) {
    this.matchesplayed = played;
  }

  public int getWon() {
    return matcheswon;
  }

  public void setWon(int won) {
    this.matcheswon = won;
  }

  public int getDrawn() {
    return matchesdrawn;
  }

  public void setDrawn(int drawn) {
    this.matchesdrawn = drawn;
  }

  public int getLost() {
    return matcheslost;
  }

  public void setLost(int lost) {
    this.matcheslost = lost;
  }

  public int getcenturies() {
	    return centuries;
	  }

  public void setcenturies(int centuries) {
	    this.centuries = centuries;
	  }

  public int getpointscored() {
	    return pointscored;
  }

  public void setpointscored(int pointscored) {
	    this.pointscored = pointscored;
  }

public int getdisqualifedperMember() {
    return disqualifedperMember;
  }

  public void setdisqualifedperMember(int disqualifedperMember) {
    this.disqualifedperMember = disqualifedperMember;
  }

  public int totalwon() {
    return totalwon;
  }

  public void settotalwon(int totalwon) {
    this.totalwon = totalwon;
  }

  public int gettotallost() {
    return totallost;
  }

  public void settotallost(int totallost) {
    this.totallost = totallost;
  }

  public int gethalfcenturies() {
    return halfcenturies;
  }

  public void sethalfcenturies(int halfcenturies) {
    this.halfcenturies = halfcenturies;
  }

  public int getsixes() {
    return sixes;
  }

  public void setsixes(int sixes) {
    this.sixes = sixes;
  }

  public int getwiketstaken() {
    return wiketstaken;
  }

  public void setwiketstaken(int wiketstaken) {
    this.wiketstaken = wiketstaken;
  }

  public int compareTo(CricketClubs C) {
    return ((Integer) disqualifedperMember).compareTo(C.disqualifedperMember);
  }
}
