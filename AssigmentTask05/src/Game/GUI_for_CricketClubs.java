package Game;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class GUI_for_CricketClubs extends JFrame {
    private JPanel contentPane;
    private JList<String> leagueList;
    private JButton viewButton;
    private JButton exitButton;
    private JTextArea outputTextArea;

    private static final String[] LEAGUE_NAMES = {
            "Mens_CricketLeague_01",
            "Mens_CricketLeague_02",
            "Mens_CricketLeague_03",
            "Mens_CricketLeague_04",
            "Mens_CricketLeague_05"
    };

    public GUI_for_CricketClubs() {
        setTitle("6CS002_Task05_2064867");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 400);
        setLocationRelativeTo(null);

        contentPane = new JPanel(new BorderLayout());
        setContentPane(contentPane);

        createUI();

        pack();
        setVisible(true);
    }

    private void createUI() {
        Font titleFont = new Font("Arial", Font.BOLD, 20);
        Font listFont = new Font("SansSerif", Font.PLAIN, 14);
        Font buttonFont = new Font("Monospaced", Font.BOLD, 16);
        Font textAreaFont = new Font("Serif", Font.PLAIN, 14);

        JLabel titleLabel = new JLabel("<html><h1><strong>Mens Cricket League</strong></h1><hr></html>");
        titleLabel.setBackground(new Color(255, 165, 0)); 
        titleLabel.setOpaque(true);
        titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
        titleLabel.setFont(titleFont);
        contentPane.add(titleLabel, BorderLayout.NORTH);

        leagueList = new JList<>(LEAGUE_NAMES);
        JScrollPane listScrollPane = new JScrollPane(leagueList);
        listScrollPane.setFont(listFont);
        contentPane.add(listScrollPane, BorderLayout.CENTER);

        viewButton = new JButton("View Selected League");
        viewButton.setBackground(new Color(0, 229, 0)); 
        viewButton.setFont(buttonFont);
        outputTextArea = new JTextArea(10, 30);
        outputTextArea.setEditable(false);
        outputTextArea.setBackground(new Color(240, 240, 240)); 
        outputTextArea.setFont(textAreaFont);
        JScrollPane textAreaScrollPane = new JScrollPane(outputTextArea);

        viewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String selectedLeague = leagueList.getSelectedValue();
                if (selectedLeague != null) {
                    outputTextArea.setText("Loading information for " + selectedLeague + "...\n");
                    switch (selectedLeague) {
                        case "Mens_CricketLeague_01":
                            outputTextArea.append("Displaying information for Mens_CricketLeague_01.\n");
                            Mens_CricketLeague_01.main(null);
                            break;
                        case "Mens_CricketLeague_02":
                            outputTextArea.append("Displaying information for Mens_CricketLeague_02.\n");
                            Mens_CricketLeague_02.main(null);
                            break;
                        case "Mens_CricketLeague_03":
                            outputTextArea.append("Displaying information for Mens_CricketLeague_03.\n");
                            Mens_CricketLeague_03.main(null);
                            break;
                        case "Mens_CricketLeague_04":
                            outputTextArea.append("Displaying information for Mens_CricketLeague_04.\n");
                            Mens_CricketLeague_04.main(null);
                            break;
                        case "Mens_CricketLeague_05":
                            outputTextArea.append("Displaying information for Mens_CricketLeague_05.\n");
                            Mens_CricketLeague_05.main(null);
                            break;
                        default:
                            outputTextArea.append("Unknown league selected.\n");
                            break;
                    }
                }
            }
        });

        exitButton = new JButton("Exit");
        exitButton.setBackground(new Color(255, 69, 0)); 
        exitButton.setFont(buttonFont);
        exitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(viewButton);
        buttonPanel.add(exitButton);

        contentPane.add(buttonPanel, BorderLayout.SOUTH);
        contentPane.add(textAreaScrollPane, BorderLayout.EAST);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new GUI_for_CricketClubs());
    }
}