package Game;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


public class Mens_CricketLeague_01 {
  public static void main(String[] args) {
    List<CricketClubs> table = Arrays.asList(
        new CricketClubs(1, "Ace Capital",         26, 18, 1, 7,  130, 4, 523, 43,  197, 257, 678, 724),
        new CricketClubs(2, "Badureliya Sports",   26, 16, 0, 7,  120, 2, 345, 129, 120, 200, 680, 734),
        new CricketClubs(3, "Bloomfield",          26, 15, 0, 11,  110, 2, 518, 49,  113, 188, 578, 643),
        new CricketClubs(4, "Burgher Recreation",  26, 15, 2, 9,  100, 2, 497, 96,   97, 167, 367, 567),
        new CricketClubs(5, "Chilaw Marians",      26, 14,  0, 12,  90,  0, 315, 186,  78, 145, 487, 583),
        new CricketClubs(6, "Colombo Cricket",     26, 13,  2, 11,  80,  3, 360, 166, 121, 201, 298, 547),
        new CricketClubs(7, "Colts Cricket",       26, 12,  0, 14,  70,  2, 478, 115,  45,  99, 501, 649),
        new CricketClubs(8, "Galle Cricket",       26, 11,  0, 15,  60,  0, 380, 286,  67, 132, 452, 596),
        new CricketClubs(9, "Kalutara Town",       26, 9,  3, 14,  50,  1, 295, 272,  78, 144, 397, 581),
        new CricketClubs(10, "Kandy Customs",      26, 8,  0, 9,  40,  2, 315, 243,  89, 187, 512, 534),
        new CricketClubs(11, "Kurunegala Youth",   26, 7,  4, 15, 30,  0, 510, 72,  100, 180, 632, 740),
        new CricketClubs(12, "Lankan Cricket",     26, 6,  0, 20, 20,  5, 365, 199,  54, 121, 586, 542),
    	new CricketClubs(13, "Moors Sports",       26, 5,  3, 21, 10,  0, 448, 125,  33, 117, 601, 693),
    	new CricketClubs(14, "Saracens Sports",    26, 4,  7, 15,  0,  3, 378, 156,  41, 136, 543, 598));
       

     System.out.println("   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  \n");
     table.forEach(c -> System.out.println(c));
    
     try {
			FileWriter writer = new FileWriter("Mens_CricketLeague_OUTPUT1.txt");
			writer.write("   Team Name                POINTS    TOTAL_WON  TOTAL_LOSS DRAWN  \n");
			writer.write("   ---------                ------    ---------  ---------- ------\n");
			table.forEach(x -> {
			try {
				writer.write(x + "\n");
			} catch (IOException c) {
				c.printStackTrace();
			}
		});
			writer.close();
			System.out.println("\nMens_CricketLeague_OUTPUT1.txt file successfully written");
	    } catch (IOException e) {
	    	System.out.println("An error has occurred.");
	    	e.printStackTrace();
	    }

  }
  

}
